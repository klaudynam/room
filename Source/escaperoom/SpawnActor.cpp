// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnActor.h"
#include "Math/UnrealMathUtility.h"
#include "Components/BoxComponent.h"
// Sets default values
ASpawnActor::ASpawnActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	Box = CreateDefaultSubobject<UBoxComponent>(FName("Box"));
	Box->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ASpawnActor::BeginPlay()
{
	Super::BeginPlay();
	
}


TArray<int32> ASpawnActor::ChoseRandomPackages(int32 HowMany, int32 AssignablePackagesNumber)
{
	if (AssignablePackagesNumber < HowMany) {
		UE_LOG(LogTemp, Warning, TEXT("Not enough packages to choose"));
		return TArray<int32>();
	}
	TArray<int32> TempArray = {};
	while (HowMany > TempArray.Num()) {
		TempArray.AddUnique(rand() % AssignablePackagesNumber + 1);
		 
	}
	return TempArray;
}

