// Fill out your copyright notice in the Description page of Project Settings.
#include "OpenDoor.h"

#include "Engine/World.h"

#include "GameFramework/Actor.h"

#define OUT
// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	if (!ActorThatOpens) { return; }
	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (!PressurePlate) { UE_LOG(LogTemp, Warning, TEXT("No pressure plate")); }
	// ...
	
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActorsOnPlate() > TriggerOpenMass) {
		OnDoorOpen.Broadcast();
	}
	else {
		OnDoorClose.Broadcast();
	}
	  
	// ...
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float MassSum = 0.f;
	TArray<AActor*> OverlappingActors;
	if (!PressurePlate) { return MassSum; }
	PressurePlate->GetOverlappingActors(OUT OverlappingActors);

	for (AActor* OverlappingActor : OverlappingActors)
	{
		//OverlappingActor->Mass
		UE_LOG(LogTemp, Warning, TEXT("%s"),*OverlappingActor->GetName());
		MassSum = MassSum + OverlappingActor->FindComponentByClass<class UPrimitiveComponent>()->GetMass();
		UE_LOG(LogTemp, Warning, TEXT("%f"), MassSum);
	}
	return MassSum;
}

